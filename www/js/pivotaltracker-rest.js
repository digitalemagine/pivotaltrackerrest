/**
 * basic Pivotal Tracker API js
 */
// pollute global space until we make this into a real module
var PivotalTrackerREST = (function() {
    'use strict';

    var PivotalTrackerREST = {},
        restVersion = 5,
        baseURL = 'https://www.pivotaltracker.com/services/v' + restVersion,
        finishedStates = ['delivered', 'finished', 'rejected'],
        workingStates = ['unstarted', 'started', 'unscheduled'];


    /**
     * creates the PivotalTracker Rest manager
     * projectId is not mandatory as it can be set per-request
     */
    PivotalTrackerREST.create = function(apiToken, projectId) {
        var self = {
            apiToken: apiToken,
            projectId: projectId
        };

        self.apiFetch = function(query, callback, projectId) {

            var projectId = projectId || self.projectId,
            	url = baseURL;
            // compose request URL
            url += '/projects/' + projectId;
            url += '/search?query=state:' + workingStates.join(',');
            // the query parsing should be smarter...
            // even the state should just be a default
            url += query && (' AND ' + query) || '';
            // url += '&limit=20';

            // alternative api to /search?query= is using /stories?filter=



            // do API request to get story names
            $.ajax({
                url: url,  //.replace(':', '%3A'),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-TrackerToken', apiToken);
                }
            }).done(callback);
        }

        return self;
    }
    return PivotalTrackerREST;
})();
